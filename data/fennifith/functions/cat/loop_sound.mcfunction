# increment time for all players
scoreboard players add @a cat.time 1

# if players are sprinting, and not playing music, restart it
# - play the sound 10k blocks above the player, with +10k volume, to minimize minecraft's uncontrollable directional audio effects
execute as @a[predicate=fennifith:cat/is_sprinting,scores={cat.time=52..}] at @s run playsound fennifith:cat.nyan music @s ~ ~-10000 ~ 10000
scoreboard players set @a[predicate=fennifith:cat/is_sprinting,scores={cat.time=52..}] cat.time 0

schedule function fennifith:cat/loop_sound 1s replace
