This resource pack replaces the chainmail helmet item/model with cat ears. These can still be enchanted and worn by players, and occasionally spawn on mobs.

![@thegroose wearing the cat ear helmet.](./.images/groose.png)
