.PHONY: build dev install clean
.DEFAULT_GOAL := dev

ifeq ($(shell test -f "$(HOME)/.nvm/nvm.sh"; echo $$?),0)
NVM := . $(HOME)/.nvm/nvm.sh && nvm use &> /dev/null
else
NVM := true
endif

ifeq (,$(shell $(NVM) && which pnpm))
NPM := $(NVM) && npm
else
NPM := $(NVM) && pnpm
endif

build: install
	$(NPM) run build

dev: install
	$(NPM) run dev

install: package-install.lock

package-install.lock: package.json
	$(NPM) install
	touch package-install.lock

clean:
	rm -rf _out/

nyan:
	ffmpeg -i nyan.ogg -af volume=0.3 assets/fennifith/sounds/cat/nyan.ogg
